(ns neanderfun.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [neanderfun.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[neanderfun started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[neanderfun has shut down successfully]=-"))
   :middleware wrap-dev})
