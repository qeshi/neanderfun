(ns neanderfun.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[neanderfun started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[neanderfun has shut down successfully]=-"))
   :middleware identity})
